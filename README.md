roman-numeral-converter
=======================

Contains methods that convert Roman Numerals to integers and vice versa.

Run against Sonar with 100% Unit Test coverage (line and branch) and 100% rules compliance (Sonar way with Findbugs).

Performed some refactoring

Build with Gradle (tested with 2.2.1)
-------------------------------------

```
gradle build
```


Build with Maven 
----------------

```
mvn package
```

Run from Command Line
---------------------
