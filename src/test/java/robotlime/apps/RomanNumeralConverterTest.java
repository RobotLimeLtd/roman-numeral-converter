package robotlime.apps;

import org.junit.*;

import static org.junit.Assert.*;

public class RomanNumeralConverterTest {

    RomanNumeralConverter romanNumeralConverter;

    @Before
    public void setUp() {
        romanNumeralConverter = new RomanNumeralConverter();
    }

    @Test
    public void testSingleNumerals() {
        assertEquals("I", romanNumeralConverter.getRoman(1));
        assertEquals(1, romanNumeralConverter.getInt("I"));

        assertEquals("V", romanNumeralConverter.getRoman(5));
        assertEquals(5, romanNumeralConverter.getInt("V"));

        assertEquals("X", romanNumeralConverter.getRoman(10));
        assertEquals(10, romanNumeralConverter.getInt("X"));


        assertEquals("L", romanNumeralConverter.getRoman(50));
        assertEquals(50, romanNumeralConverter.getInt("L"));


        assertEquals("C", romanNumeralConverter.getRoman(100));
        assertEquals(100, romanNumeralConverter.getInt("C"));


        assertEquals("D", romanNumeralConverter.getRoman(500));
        assertEquals(500, romanNumeralConverter.getInt("D"));


        assertEquals("M", romanNumeralConverter.getRoman(1000));
        assertEquals(1000, romanNumeralConverter.getInt("M"));
    }

    @Test
    public void testExampleNumbersFromSpecs() {
        assertEquals("VII", romanNumeralConverter.getRoman(7));
        assertEquals("MCMXCIX", romanNumeralConverter.getRoman(1999));
        assertEquals("MCMXC", romanNumeralConverter.getRoman(1990));
        assertEquals("MMVIII", romanNumeralConverter.getRoman(2008));
    }

    @Test
    public void testRepeatingNumbers() {
        assertEquals("II", romanNumeralConverter.getRoman(2));
        assertEquals(2, romanNumeralConverter.getInt("II"));

        assertEquals("III", romanNumeralConverter.getRoman(3));
        assertEquals(3, romanNumeralConverter.getInt("III"));

        assertEquals("MMM", romanNumeralConverter.getRoman(3000));
        assertEquals(3000, romanNumeralConverter.getInt("MMM"));
    }

    @Test
    public void testAwkwardNumberPairs() {
        assertEquals("IV", romanNumeralConverter.getRoman(4));
        assertEquals(4, romanNumeralConverter.getInt("IV"));

        assertEquals("IX", romanNumeralConverter.getRoman(9));
        assertEquals(9, romanNumeralConverter.getInt("IX"));

        assertEquals("XL", romanNumeralConverter.getRoman(40));
        assertEquals(40, romanNumeralConverter.getInt("XL"));

        assertEquals("XC", romanNumeralConverter.getRoman(90));
        assertEquals(90, romanNumeralConverter.getInt("XC"));

        assertEquals("CD", romanNumeralConverter.getRoman(400));
        assertEquals(400, romanNumeralConverter.getInt("CD"));

        assertEquals("CM", romanNumeralConverter.getRoman(900));
        assertEquals(900, romanNumeralConverter.getInt("CM"));

    }

    @Test
    public void testComplexSequences() {
        assertEquals("MLXVI", romanNumeralConverter.getRoman(1066));
        assertEquals(1066, romanNumeralConverter.getInt("MLXVI"));

        assertEquals("MDCLXIV", romanNumeralConverter.getRoman(1664));
        assertEquals(1664, romanNumeralConverter.getInt("MDCLXIV"));

        assertEquals("MCDXL", romanNumeralConverter.getRoman(1440));
        assertEquals(1440, romanNumeralConverter.getInt("MCDXL"));

        assertEquals("MCMXC", romanNumeralConverter.getRoman(1990));
        assertEquals(1990, romanNumeralConverter.getInt("MCMXC"));

        assertEquals("MCMXCIX", romanNumeralConverter.getRoman(1999));
        assertEquals(1999, romanNumeralConverter.getInt("MCMXCIX"));

    }

    @Test (expected = IllegalArgumentException.class)
    public void testNegativeInteger() {
        romanNumeralConverter.getRoman(-1);
    }


    @Test (expected = IllegalArgumentException.class)
    public void testZero() {
        romanNumeralConverter.getRoman(0);
    }


    @Test (expected = IllegalArgumentException.class)
    public void testGreaterThanThreeThousand() {
        romanNumeralConverter.getRoman(3001);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testBadRomanCharacters() {
        romanNumeralConverter.getInt("DOG");
    }

    @Test (expected = IllegalArgumentException.class)
    public void testBadRomanCharacterPair() {
        romanNumeralConverter.getInt("IM");
    }


}
