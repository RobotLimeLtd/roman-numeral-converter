package robotlime.apps;

public class Main {
  public static void main(String[] args) {

    RomanNumeralConverter romanNumeralConverter = new RomanNumeralConverter();

    String result;

    if (args.length > 0) {
      String firstArg = args[0];
      if (isNumeric(firstArg)) {
        int num = Integer.parseInt(firstArg);
        result = romanNumeralConverter.getRoman(num);
      } else {
        result = "" + romanNumeralConverter.getInt(firstArg.toUpperCase());
      }
    } else {
      throw new IllegalArgumentException("You must provide an argument!");
    }

    System.out.println(result);

  }

  private static boolean isNumeric(String s) {
    return java.util.regex.Pattern.matches("\\d+", s);
  }

}
