package robotlime.apps;

public class RomanNumeralConverter {

  private static final int MAX_VALUE = 3000;
  private static final int MIN_VALUE = 1;

  /**
   * Converts a decimal integer into a Roman Numeral.
   *
   * @param suppliedNumber any decimal integer in the range 1 - 3000
   * @return the corresponding Roman Numeral
   */
  public String getRoman(int suppliedNumber) {
    StringBuilder romanNumber = new StringBuilder();

    if (isOutsideRange(suppliedNumber)) {
      throw new IllegalArgumentException("Valid numbers are in the range 1 - 3,000");
    }

    int remainder = suppliedNumber;

    for (RomanNumeral numeral : RomanNumeral.values()) {
      while (remainder > 0 && numeral.getDecimalValue() <= remainder) {
        romanNumber.append(numeral.toString());
        remainder -= numeral.getDecimalValue();
      }
    }
    return romanNumber.toString();
  }

  /**
   * Converts a Roman Numeral to a decimal integer
   *
   * @param romanNumeral any valid Roman Numeral in the range I - MMM
   * @return the corresponding integer
   */
  public int getInt(String romanNumeral) {
    int result = 0;

    for (int i = 0; i < romanNumeral.length(); i++) {

      String firstCharacter = String.valueOf(romanNumeral.charAt(i));
      validateRomanNumeral(firstCharacter);

      String nextCharacter = "";
      if (i < (romanNumeral.length() - 1)) {
        nextCharacter = String.valueOf(romanNumeral.charAt(i + 1));
        validateRomanNumeral(nextCharacter);
      }

      if (!"".equals(nextCharacter)
          && RomanNumeral.valueOf(nextCharacter).getDecimalValue()
          > RomanNumeral.valueOf(firstCharacter).getDecimalValue()) {
        String characterPair = firstCharacter + nextCharacter;
        validateRomanNumeral(characterPair);
        result += RomanNumeral.valueOf(characterPair).getDecimalValue();
        i++;
      } else {
        // It's a single
        result += RomanNumeral.valueOf(firstCharacter).getDecimalValue();
      }

    }


    return result;
  }

  private boolean isOutsideRange(int number) {
    return (number < MIN_VALUE || number > MAX_VALUE);
  }

  private void validateRomanNumeral(String s) {
    try {
      RomanNumeral.valueOf(s);
    } catch (IllegalArgumentException ex) {
      throw new IllegalArgumentException("Illegal Character " + s + " detected: ", ex);
    }

  }
}
